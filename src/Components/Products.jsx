import React from "react";
import { Box } from "@mui/system";
import { Typography } from "@mui/material";
const Products = ({ product }) => {
  return (
    <Box
      display="flex"
      flexDirection="row"
      backgroundColor="#232323"
      color="white"
      m={2}
      borderRadius={2}
      p={3}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          width: "80%",
        }}
      >
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
          }}
        >
          <img src={product.image} alt="product" height="70px" width="70px" />
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
            }}
          >
            <Box display="flex" flexDirection="column">
              <Typography>{product.product_name}</Typography>
              <Typography color="#9E9E9E">{product.brand_name}</Typography>
              <Typography>${product.price}</Typography>
            </Box>
          </Box>
        </Box>
        <Box
          sx={{
            display: "flex",
            flexDirection: "row",
            justifyContent: "space-between",
            color: "#9E9E9E",
            width: "250px",
          }}
        >
          <Typography>
            {product.address.city} {product.address.state}
          </Typography>
          <Typography>
            Date : {new Date(product.date).toLocaleDateString("en-US")}
          </Typography>
        </Box>
        <Typography color="#9E9E9E">{product.discription}</Typography>
      </Box>
    </Box>
  );
};

export default Products;
