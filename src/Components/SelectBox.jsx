import React from "react";

const SelectBox = ({ data, name, filterTypes, setFilterTypes }) => {
  const handleChange = (e) => {
    const { name, value } = e.target;
    setFilterTypes((prevState) => {
      return {
        ...prevState,
        [name]: value,
      };
    });
  };
  return (
    <select
      name={name}
      style={{
        height: "5vh",
        width: "150px",
        background: "#232323",
        color: "white",
        cursor: "pointer",
        marginTop: 15,
        borderRadius: 5,
        borderColor: "transparent",
        padding: 5,
      }}
      onChange={handleChange}
    >
      <option value={name} selected disabled hidden>
        {name}
      </option>
      {data &&
        data.map((item, idx) => (
          <option key={idx} value={item}>
            {item}
          </option>
        ))}
    </select>
  );
};

export default SelectBox;
