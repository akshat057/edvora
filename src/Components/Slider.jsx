import React from "react";
import { Swiper, SwiperSlide } from "swiper/react/swiper-react";

import "swiper/swiper-bundle.min.css";

import SwiperCore, { Navigation, Mousewheel, Keyboard } from "swiper";
import Products from "./Products";

SwiperCore.use([Navigation, Mousewheel, Keyboard]);

const Slider = ({ data, filterTypes }) => {
  return (
    <Swiper
      cssMode={true}
      mousewheel={true}
      keyboard={true}
      slidesPerView={3}
      navigation={true}
      className="mySwiper"
    >
      {data.map((item, idx) => {
        if (
          item.address.state
            .toLowerCase()
            .includes(filterTypes.State.toLowerCase()) &&
          item.address.city
            .toLowerCase()
            .includes(filterTypes.City.toLowerCase())
        )
          return (
            <SwiperSlide key={idx}>
              <Products product={item} />
            </SwiperSlide>
          );
        else {
          return null;
        }
      })}
    </Swiper>
  );
};

export default Slider;
