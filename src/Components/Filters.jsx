import React from "react";
import { Box } from "@mui/system";
import { Button, Divider, Typography } from "@mui/material";
import SelectBox from "./SelectBox";

const Filters = ({ data, state, city, filterTypes, setFilterTypes }) => {
  return (
    <Box
      sx={{ width: "150px", height: "200px", padding: 3, background: "black" }}
      borderRadius={3}
    >
      <Box display="flex" justifyContent="space-between">
        <Typography
          variant="subtitle1"
          color="#A5A5A5"
          backgroundColor="transparent"
        >
          Filters
        </Typography>
        <Button
          variant="outlined"
          size="small"
          onClick={() =>
            setFilterTypes((prevState) => ({
              ...prevState,
              Products: "",
              State: "",
              City: "",
            }))
          }
        >
          {" "}
          Clear
        </Button>
      </Box>
      <Divider style={{ background: "#cbcbcb", marginTop: 10 }} />
      <SelectBox
        data={Object.keys(data).map((item) => item)}
        name="Products"
        filterTypes={filterTypes}
        setFilterTypes={setFilterTypes}
      />
      <SelectBox
        data={state}
        name="State"
        filterTypes={filterTypes}
        setFilterTypes={setFilterTypes}
      />
      <SelectBox
        data={city}
        name="City"
        filterTypes={filterTypes}
        setFilterTypes={setFilterTypes}
      />
    </Box>
  );
};

export default Filters;
