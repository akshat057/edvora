import "./App.css";
import axios from "axios";
import React from "react";
import { Box } from "@mui/system";
import { Divider, Typography } from "@mui/material";
import Filters from "./Components/Filters";
import Slider from "./Components/Slider";

function App() {
  const [data, setData] = React.useState({});
  const [city, setCity] = React.useState([]);
  const [state, setState] = React.useState([]);
  const [filterTypes, setFilterTypes] = React.useState({
    Products: "",
    State: "",
    City: "",
  });
  React.useEffect(() => {
    const callApi = async () => {
      const { data } = await axios.get(
        "https://assessment-edvora.herokuapp.com/"
      );
      let arr = data.map(({ product_name }) => product_name);
      arr = [...new Set(arr)];
      let state = data.map((item) => item.address.state);
      state = [...new Set(state)];
      let city = data.map((item) => item.address.city);
      city = [...new Set(city)];

      let products = {};
      arr.forEach((item) => (products[item] = []));
      data.forEach((item) => products[item.product_name].push(item));
      Object.keys(products).length > 0 && setData(products);
      setState(state);
      setCity(city);
    };
    callApi();
  }, []);

  // console.log(data);
  return (
    <Box
      sx={{
        minHeight: "100vh",
        minWidth: "100vw",
        display: "flex",
        flexDirection: "row",
        background: "#232323",
        flexWrap: "wrap",
      }}
    >
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          padding: 3,
          flex: 0.2,
        }}
      >
        <Filters
          data={data}
          state={state}
          city={city}
          filterTypes={filterTypes}
          setFilterTypes={setFilterTypes}
        />
      </Box>
      <Box
        sx={{
          display: "flex",
          flexDirection: "column",
          flex: 0.8,
          // border: "1px solid red",
          padding: "2em",
        }}
      >
        <Typography variant="h3" color="white">
          Edvora
        </Typography>
        <Typography variant="h5" color="#9E9E9E" mt={2}>
          Products
        </Typography>
        {Object.keys(data).length <= 0 && (
          <Typography variant="h5" color="#fff" mt={5}>
            Loading...
          </Typography>
        )}
        {Object.keys(data).length > 0 &&
          Object.keys(data).map((item, index) => {
            if (
              item.toLowerCase().includes(filterTypes.Products.toLowerCase())
            ) {
              return (
                <Box
                  sx={{
                    display: "flex",
                    flexDirection: "column",
                    width: "70vw",
                  }}
                  key={`${item}${index}`}
                  color="white"
                  marginTop={5}
                >
                  <Typography variant="h5" mb={2}>
                    {item}
                  </Typography>
                  <Divider
                    style={{ background: "#CBCBCB", marginBottom: 10 }}
                  />
                  <Box backgroundColor="#000" width="70vw" borderRadius={4}>
                    <Slider data={data[item]} filterTypes={filterTypes} />
                  </Box>
                </Box>
              );
            } else {
              return null;
            }
          })}
      </Box>
    </Box>
  );
}

export default App;
